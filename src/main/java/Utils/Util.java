package Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Util {
    public static String getValueFromProperties(String key) {
        FileInputStream fis = null;
        Properties property = new Properties();
        String path = "";

        try {
            fis = new FileInputStream("src/main/resources/properties.properties");

        } catch (FileNotFoundException e) {
            System.err.println("ОШИБКА: Файл свойств отсутствует!");
        }
        try {
            property.load(fis);
        } catch (IOException e) {
            System.err.println("ОШИБКА: значение не найдено!");
        }
        path = property.getProperty(key);
        return path;
    }
}
