import Utils.Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        String path = Util.getValueFromProperties("path");
        String resultFileName = Util.getValueFromProperties("resultFileName");

        List<File> list = Files.walk(Paths.get(path))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .filter(file -> file.getPath().endsWith(".txt"))
                .sorted(Comparator.comparing(File::getName))
                .collect(Collectors.toList());

        System.out.println("В директории " + path + " найдены следующие файлы с расширением txt: ");

        for (File file : list) {
            System.out.println(file.getName());
        }
        File resultFile = new File(path + "\\" + resultFileName + ".txt");

        PrintWriter printWriter = new PrintWriter(resultFile);
        for (File file : list) {
            try (FileReader reader = new FileReader(file);
                 BufferedReader bufferedReader = new BufferedReader(reader)) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    printWriter.println(line);
                }
            }

        }
        printWriter.close();
        System.out.println("Информация из файлов сохранена в " + resultFile);
    }
}
